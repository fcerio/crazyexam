package com.franciscerio;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.franciscerio.api.APIRequestManager;
import com.franciscerio.api.RequestResponseListener;
import com.franciscerio.common.CommonValues;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends ActionBarActivity {

    private ArrayList<String> mCategoryList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        fetchCategories();

    }

    @OnClick(R.id.btnNameJoke)
    public void OnClickBtnName(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_name_jokes);
        final EditText etFirstName = (EditText) dialog.findViewById(R.id.etFirstName);
        final EditText etLastName = (EditText) dialog.findViewById(R.id.etLastName);
        Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etFirstName.getText().toString().isEmpty() || etFirstName.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "Firstname needed", Toast.LENGTH_SHORT).show();
                } else if (etLastName.getText().toString().isEmpty() || etLastName.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "Lastname needed", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(MainActivity.this, JokesActivity.class);
                    intent.putExtra("type", CommonValues.NAME_JOKES);
                    intent.putExtra("firstname", etFirstName.getText().toString());
                    intent.putExtra("lastname", etLastName.getText().toString());
                    startActivity(intent);
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @OnClick(R.id.btnRandomJoke)
    public void OnClickBtnRandom(View view) {
        Intent intent = new Intent(MainActivity.this, JokesActivity.class);
        intent.putExtra("type", CommonValues.RANDOM_JOKES);
        startActivity(intent);
    }

    @OnClick(R.id.btnCategoryJoke)
    public void OnClickBtnCategory(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_category_jokes);
        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);

        for (int i = 0; i < mCategoryList.size(); i++) {
            RadioButton radioButton = new RadioButton(MainActivity.this);
            radioButton.setId(i);
            radioButton.setText(mCategoryList.get(i).toLowerCase());
            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            radioGroup.addView(radioButton, params);
        }

        Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioGroup.getCheckedRadioButtonId() > -1) {
                    int selectedId = radioGroup.getCheckedRadioButtonId();
                    RadioButton radioButton = (RadioButton) dialog.findViewById(selectedId);
                    Intent intent = new Intent(MainActivity.this, JokesActivity.class);
                    intent.putExtra("type", CommonValues.CATEGORY_JOKES);
                    intent.putExtra("joke_category", radioButton.getText().toString().toLowerCase());
                    startActivity(intent);
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void fetchCategories() {
        mCategoryList = new ArrayList<>();

        Toast.makeText(MainActivity.this, "Fetching Categories", Toast.LENGTH_SHORT).show();

        APIRequestManager.getCategoryJokes(new RequestResponseListener() {
            @Override
            public void requestCompleted(JSONObject response) {
                try {
                    JSONArray arrayObj = response.getJSONArray("value");
                    for (int i = 0; i < arrayObj.length(); i++) {
                        String value = (String) arrayObj.get(i);
                        mCategoryList.add(value);
                    }
                    Toast.makeText(MainActivity.this, "Fetching Categories Done!", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void requestEndedWithError(VolleyError error) {

            }
        });

    }

}
