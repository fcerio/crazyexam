package com.franciscerio;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.franciscerio.api.APIRequestManager;
import com.franciscerio.api.RequestResponseListener;
import com.franciscerio.common.CommonValues;
import com.franciscerio.models.Joke;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * Created by Cerio on 4/15/15.
 */
public class JokesActivity extends ActionBarActivity {

    @InjectView(R.id.textView)
    TextView mTvText;

    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jokes);
        ButterKnife.inject(this);
        init();
    }

    public void init() {
        mTvText.setText("Please wait...");

        if (getIntent().getExtras() != null) {
            type = getIntent().getExtras().getString("type", "");

            if (!type.isEmpty()) {
                if (type.equalsIgnoreCase(CommonValues.NAME_JOKES)) {
                    String firstName = getIntent().getExtras().getString("firstname", "");
                    String lastName = getIntent().getExtras().getString("lastname", "");

                    APIRequestManager.getNameJokes(firstName, lastName, new RequestResponseListener() {
                        @Override
                        public void requestCompleted(JSONObject response) {
                            Log.e("response", "" + response.toString());
                            try {
                                Joke joke = new Gson().fromJson(response.getJSONObject("value").toString(), Joke.class);
                                if (joke != null) {
                                    mTvText.setText(Html.fromHtml(joke.getJoke()));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void requestEndedWithError(VolleyError error) {
                        }
                    });
                } else if (type.equalsIgnoreCase(CommonValues.RANDOM_JOKES)) {

                    APIRequestManager.getRandomJokes(new RequestResponseListener() {
                        @Override
                        public void requestCompleted(JSONObject response) {
                            try {
                                Joke joke = new Gson().fromJson(response.getJSONObject("value").toString(), Joke.class);
                                if (joke != null) {
                                    mTvText.setText(Html.fromHtml(joke.getJoke()));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void requestEndedWithError(VolleyError error) {
                        }
                    });

                } else {
                    //Category Jokes
                    String jokeCategory = getIntent().getExtras().getString("joke_category", "");
                    if (!jokeCategory.isEmpty()) {
                        APIRequestManager.getCategoryTypeJokes(jokeCategory, new RequestResponseListener() {
                            @Override
                            public void requestCompleted(JSONObject response) {

                                try {
                                    Joke joke = new Gson().fromJson(response.getJSONObject("value").toString(), Joke.class);
                                    if (joke != null) {
                                        mTvText.setText(Html.fromHtml(joke.getJoke()));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void requestEndedWithError(VolleyError error) {

                            }
                        });
                    }

                }
            }
        }
    }
}
