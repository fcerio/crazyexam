package com.franciscerio.api;

import android.util.Log;

import com.franciscerio.api.request.GetRequest;

/**
 * Created by Cerio on 4/15/15.
 */
public class APIRequestManager {

    public static String HOST = "http://api.icndb.com";
    public static String API = "jokes";

    public static final String URL_RANDOM_JOKES = "%s/%s/random";
    public static final String URL_NAME_JOKES = "%s/%s/random?firstName=%s&lastName=%s";
    public static final String URL_CATEGORY_TYPE_JOKES = "%s/%s/random?limitTo=[%s]";
    public static final String URL_CATEGORY_JOKE = "http://api.icndb.com/categories";

    public static void getRandomJokes(
            final RequestResponseListener listener) {

        String urlPath = String
                .format(APIRequestManager.URL_RANDOM_JOKES,
                        APIRequestManager.HOST,
                        APIRequestManager.API);

        Log.e("url", urlPath);
        new GetRequest(urlPath, listener)
                .sendRequest();
    }

    public static void getNameJokes(String firstName, String lastName,
                                    final RequestResponseListener listener) {
        String urlPath = String
                .format(APIRequestManager.URL_NAME_JOKES,
                        APIRequestManager.HOST,
                        APIRequestManager.API, firstName, lastName);

        Log.e("url", urlPath);
        new GetRequest(urlPath, listener)
                .sendRequest();
    }

    public static void getCategoryTypeJokes(String category,
                                            final RequestResponseListener listener) {
        String urlPath = String
                .format(APIRequestManager.URL_CATEGORY_TYPE_JOKES,
                        APIRequestManager.HOST,
                        APIRequestManager.API, category);

        Log.e("url", urlPath);
        new GetRequest(urlPath, listener)
                .sendRequest();
    }

    public static void getCategoryJokes(
            final RequestResponseListener listener) {
        String urlPath = String
                .format(APIRequestManager.URL_CATEGORY_JOKE,
                        APIRequestManager.HOST,
                        APIRequestManager.API);

        Log.e("url", urlPath);
        new GetRequest(urlPath, listener)
                .sendRequest();
    }

}
