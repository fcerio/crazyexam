package com.franciscerio.api.request;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.franciscerio.FrancisApplication;
import com.franciscerio.api.CJsonObjectRequest;
import com.franciscerio.api.RequestResponseListener;

import org.json.JSONObject;

public class GetRequest {

    protected CJsonObjectRequest mRequest;
    protected RequestResponseListener mListener;
    protected String mUrl;

    public GetRequest(String urlPath, RequestResponseListener listener) {
        mUrl = urlPath;
        mListener = listener;
    }

    public void sendRequest() {
        mRequest = new CJsonObjectRequest(Request.Method.GET, mUrl,null,
                new ResponseListener(), new ErrorListener());
        FrancisApplication.getInstance().addToRequestQueue(mRequest);
    }


    protected class ResponseListener implements Response.Listener<JSONObject> {
        @Override
        public void onResponse(JSONObject response) {
            if (mListener != null)
                mListener.requestCompleted(response);
        }
    }

    protected class ErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (mListener != null)
                mListener.requestEndedWithError(error);
        }

    }

}