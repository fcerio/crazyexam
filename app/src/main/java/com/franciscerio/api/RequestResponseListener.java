package com.franciscerio.api;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface RequestResponseListener {
    public void requestCompleted(JSONObject response);
	public void requestEndedWithError(VolleyError error);
}