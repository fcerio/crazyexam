package com.franciscerio.models;

/**
 * Created by Cerio on 4/15/15.
 */
public class Joke {

    private int id;
    private String joke;

    public int getId() {
        return id;
    }

    public String getJoke() {
        return joke;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setJoke(String joke) {
        this.joke = joke;
    }

}
